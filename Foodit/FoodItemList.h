//
//  FoodItemList.h
//  Foodit
//
//  Created by Roy Josefsson on 10/03/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodItemList : NSObject
@property (nonatomic)NSMutableDictionary* food;

+ (FoodItemList*) getInstance;

@end

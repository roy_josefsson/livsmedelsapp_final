//
//  AboutAppViewController.m
//  Foodit
//
//  Created by Roy Josefsson on 29/03/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import "AboutAppViewController.h"

@interface AboutAppViewController ()

@property (nonatomic) UIDynamicAnimator* animator;
@property (nonatomic) UIGravityBehavior* gravity;
@property (nonatomic) UICollisionBehavior* collision;

@end

@implementation AboutAppViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView* square = [[UIView alloc]initWithFrame:CGRectMake(200, 200, 100, 100)];
    square.backgroundColor = [UIColor grayColor];
    [self.view addSubview:square];
    
    self.animator = [[UIDynamicAnimator alloc]initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc]initWithItems:@[square]];
    self.collision = [[UICollisionBehavior alloc]initWithItems:@[square]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    
    [self.animator addBehavior:self.gravity];
    [self.animator addBehavior:self.collision];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

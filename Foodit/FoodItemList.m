//
//  FoodItemList.m
//  Foodit
//
//  Created by Roy Josefsson on 10/03/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import "FoodItemList.h"

@implementation FoodItemList

static FoodItemList* instance;

+ (FoodItemList*) getInstance{
    NSLog(@"Instance #########################################")
    ;
    if(instance == nil){
        instance = [[FoodItemList alloc]init];
    }
    return instance;
}

@end

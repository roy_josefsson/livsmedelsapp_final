//
//  InfoViewController.m
//  Foodit
//
//  Created by Roy Josefsson on 10/03/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import "InfoViewController.h"
#import "FoodTableViewCell.h"

@interface InfoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *proteinValue;
@property (weak, nonatomic) IBOutlet UILabel *fatValue;
@property (weak, nonatomic) IBOutlet UILabel *vitaminE;
@property (weak, nonatomic) IBOutlet UILabel *vitaminC;
@property (weak, nonatomic) IBOutlet UILabel *vitaminB6;
@property (weak, nonatomic) IBOutlet UILabel *vitaminB16;
@property (weak, nonatomic) IBOutlet UILabel *vitaminD;
@property (weak, nonatomic) IBOutlet UILabel *healthScore;

@end

@implementation InfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.proteinValue.text = [NSString stringWithFormat:@"%@", self.food[@"nutrientValues"][@"protein"]];
    self.fatValue.text = [NSString stringWithFormat:@"%@", self.food[@"nutrientValues"][@"fat"]];
    self.vitaminD.text = [NSString stringWithFormat:@"%@", self.food[@"nutrientValues"][@"vitaminD"]];
    self.vitaminE.text = [NSString stringWithFormat:@"%@", self.food[@"nutrientValues"][@"vitaminE"]];
    self.vitaminC.text = [NSString stringWithFormat:@"%@", self.food[@"nutrientValues"][@"vitaminC"]];
    self.vitaminB6.text = [NSString stringWithFormat:@"%@", self.food[@"nutrientValues"][@"vitaminB6"]];
    self.vitaminB16.text = [NSString stringWithFormat:@"%@", self.food[@"nutrientValues"][@"vitaminB12"]];
    [self generateScore];
    [self changeToGray];
}

-(void)generateScore{
    NSNumber* score = @(
    [self.food[@"nutrientValues"][@"protein"] intValue] +
    [self.food[@"nutrientValues"][@"fat"] intValue] +
    [self.food[@"nutrientValues"][@"vitaminD"] intValue] +
    [self.food[@"nutrientValues"][@"vitaminE"] intValue] +
    [self.food[@"nutrientValues"][@"vitaminC"] intValue] +
    [self.food[@"nutrientValues"][@"vitaminB6"] intValue] +
    [self.food[@"nutrientValues"][@"vitaminB12"] intValue]);
    self.healthScore.text = [NSString stringWithFormat: [score stringValue]];
}

-(void)changeToWhite{
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options:nil
                     animations:^{
                         self.view.backgroundColor = [UIColor whiteColor];
                     }
                     completion:^(BOOL finished){
                         [self changeToGray];
                     }];
}

-(void)changeToGray{
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options:nil
                     animations:^{
                         self.view.backgroundColor = [UIColor grayColor];
                     }
                     completion:^(BOOL finished){
                         [self changeToWhite];
                     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

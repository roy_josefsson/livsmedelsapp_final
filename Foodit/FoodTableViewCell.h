//
//  FoodTableViewCell.h
//  Foodit
//
//  Created by Roy Josefsson on 10/03/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodTableViewCell : UITableViewCell

@property (nonatomic)NSDictionary* food;

-(instancetype)init;

@end

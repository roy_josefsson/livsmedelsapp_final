//
//  FoodsTableViewController.m
//  Foodit
//
//  Created by Roy Josefsson on 27/02/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import "FoodsTableViewController.h"
#import "FoodTableViewCell.h"
#import "InfoViewController.h"

@interface FoodsTableViewController ()
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (nonatomic)NSMutableArray* foodNumbers;
@property (nonatomic)int indexPathRow;

@end

@implementation FoodsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.foodNumbers = [[NSMutableArray alloc]init];
    
}

- (IBAction)searchButton:(id)sender {
    if(![self.searchField.text  isEqual: @""]){
        self.foodNumbers = @[].mutableCopy;
        self.foods = @[].mutableCopy;
        
        [self.view endEditing:YES];
        
        NSString* s = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@", self.searchField.text];
        NSString* escape = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL* url = [NSURL URLWithString:escape];
        
        NSURLRequest* request = [NSURLRequest requestWithURL:url];
        NSURLSession* session = [NSURLSession sharedSession];
        
        NSURLSessionTask* task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData* data, NSURLResponse* response, NSError* error) {
                                                
                                                if(error){
                                                    NSLog(@"%@", error);
                                                    return;
                                                }
                                                
                                                NSError* jsonParseError = nil;
                                                
                                                NSMutableArray* result = [[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&  jsonParseError]mutableCopy];
                                                
                                                if(jsonParseError){
                                                    NSLog(@"%@", jsonParseError);
                                                    return;
                                                }
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    
                                                    if(result.count == 0){
                                                        NSLog(@"No related topics");
                                                    } else{
                                                        for(int i = 0; i < result.count; i++){
                                                            [self.foodNumbers addObject:result[i][@"number"]];
                                                            
                                                        }
                                                    }
                                                    for(int i = 0; i<self.foodNumbers.count; i++){
                                                        [self runFoodWithNumber:result[i][@"number"]];
                                                    }
                                                    self.searchField.text = @"";
                                                });
                                            }];
        [task resume];
    }
}

-(void)runFoodWithNumber:(NSString*)foodNumber{
    NSString* newS = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", foodNumber];
    
    NSURL* newUrl = [NSURL URLWithString:newS];
    NSURLRequest* newRequest = [NSURLRequest requestWithURL:newUrl];
    NSURLSession* newSession = [NSURLSession sharedSession];
    
    NSURLSessionTask* newTask = [newSession dataTaskWithRequest:newRequest
                                              completionHandler:^(NSData* data, NSURLResponse* response, NSError* error) {
                                                  
                                                  if(error){
                                                      NSLog(@"%@", error);
                                                      return;
                                                  }
                                                  
                                                  NSError* jsonParseError = nil;
                                                  
                                                  NSMutableDictionary* newResult = [[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError]mutableCopy];
                                                  // -   NSLog(@"%@", newResult);
                                                  
                                                  if(jsonParseError){
                                                      NSLog(@"%@", jsonParseError);
                                                      return;
                                                  }
                                                  
                                                  [self.foods addObject:newResult];
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [self.tableView reloadData];
                                                  });
                                              }];
    [newTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.foods.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FoodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"
                                                            forIndexPath:indexPath];
    
    cell.textLabel.text = self.foods[indexPath.row][@"name"];
    cell.food = self.foods[indexPath.row];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(FoodTableViewCell*)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    InfoViewController* destination = [segue destinationViewController];
    destination.title = sender.textLabel.text;
    destination.food = sender.food;
}






@end

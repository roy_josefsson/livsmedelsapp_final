//
//  FoodTableViewCell.m
//  Foodit
//
//  Created by Roy Josefsson on 10/03/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import "FoodTableViewCell.h"
#import "FoodItemList.h"//;

@implementation FoodTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

static FoodItemList* instance;

+(instancetype)init{
    if(instance == nil){
        instance = [[FoodItemList alloc]init];
    }
    return instance;
}

@end

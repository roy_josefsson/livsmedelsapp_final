//
//  FoodsTableViewController.h
//  Foodit
//
//  Created by Roy Josefsson on 27/02/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodsTableViewController : UITableViewController

@property (nonatomic)NSMutableArray* foods;

@end

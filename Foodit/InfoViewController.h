//
//  InfoViewController.h
//  Foodit
//
//  Created by Roy Josefsson on 10/03/16.
//  Copyright © 2016 Roy Josefsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoViewController : UIViewController

@property (nonatomic)NSDictionary* food;

@end
